import Vue from "vue";

/* eslint-disable-next-line no-new */
new Vue({
	el: "#app",
	/* the elements are closed out-of-order and should yield an error */
	template: "<p>Lorem ipsum</i>",
});
