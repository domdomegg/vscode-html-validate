/* eslint-disable no-console, no-process-exit */

import path from "path";
import { runTests } from "vscode-test";

const userDataDir = path.resolve(__dirname, "../../../profile");

async function main(): Promise<void> {
	const extensionDevelopmentPath = path.resolve(__dirname, "../../../");
	const extensionTestsPath = path.resolve(__dirname, "./index");

	try {
		await runTests({
			version: process.env.VSCODE_VERSION || "stable",
			extensionDevelopmentPath,
			extensionTestsPath,
			launchArgs: [
				`--user-data-dir=${userDataDir}`,

				/* disable other extensions so they dont interfere (e.g. eslint for javascript files) */
				"--disable-extensions",
			],
		});
	} catch (err) {
		console.error(err);
		console.error("Failed to run tests");
		process.exit(1);
	}
}

main();
