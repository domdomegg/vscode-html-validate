/* eslint-disable import/no-mutable-exports */

import path from "path";

/* eslint-disable-next-line import/no-unresolved */
import vscode, { Uri } from "vscode";

export let doc: vscode.TextDocument;
export let editor: vscode.TextEditor;
export let documentEol: string;
export let platformEol: string;

export async function activate(docUri: vscode.Uri): Promise<void> {
	const ext = vscode.extensions.getExtension("html-validate.vscode-html-validate");
	await ext.activate();
	try {
		doc = await vscode.workspace.openTextDocument(docUri);
		editor = await vscode.window.showTextDocument(doc);
		await sleep(10000); // Wait for server activation
	} catch (e) {
		/* eslint-disable-next-line no-console */
		console.error(e);
	}
}

async function sleep(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export const getDocPath = (p: string): string => {
	return path.resolve(__dirname, "../../../examples", p);
};

export const getDocUri = (p: string): Uri => {
	return vscode.Uri.file(getDocPath(p));
};

export async function setTestContent(content: string): Promise<boolean> {
	const all = new vscode.Range(doc.positionAt(0), doc.positionAt(doc.getText().length));
	return editor.edit((eb) => eb.replace(all, content));
}
