import path from "path";

import vscode, {
	workspace,
	ExtensionContext,
	window as Window,
	MessageItem,
} from "vscode"; /* eslint-disable-line import/no-unresolved */

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind,
	RequestType,
} from "vscode-languageclient/node";

import * as commands from "./commands";

interface ButtonItem extends MessageItem {
	id: number;
}

interface MinVersionErrorParams {
	min: string;
	cur: string;
}

interface ConfigErrorParams {
	error: string;
}

const MissingLibraryErrorRequest = new RequestType<void, void, void>(
	"html-validate/missing-library-error"
);

const MinVersionErrorRequest = new RequestType<MinVersionErrorParams, void, void>(
	"html-validate/min-version-error"
);

const ConfigErrorRequest = new RequestType<ConfigErrorParams, void, void>(
	"html-validate/config-error"
);

let client: LanguageClient;

export function activate(context: ExtensionContext): void {
	const serverModule = context.asAbsolutePath(path.join("server", "out", "server.js"));

	const serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {
			module: serverModule,
			transport: TransportKind.ipc,
			options: { execArgv: ["--nolazy", "--inspect=6009"] },
		},
	};

	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: "file" }],
		synchronize: {
			fileEvents: [
				workspace.createFileSystemWatcher("**/.htmlvalidate.json"),
				workspace.createFileSystemWatcher("**/package.json"),

				/** TODO should look at .htmlvalidate.json to determine if any custom
				 * element metadata is present and watch those files in particular, no
				 * matter the filename */
				workspace.createFileSystemWatcher("**/elements.json"),
			],
		},
	};

	client = new LanguageClient("html-validate", "HTML-Validate", serverOptions, clientOptions);

	client.onReady().then(() => {
		/* show error if the html-validate library could not be found */
		client.onRequest(MissingLibraryErrorRequest, handleMissingLibraryError);

		/* show error if the client html-validate version isn't recent enough */
		client.onRequest(MinVersionErrorRequest, handleMinVersionError);

		/* show error if a configuration error was detected */
		client.onRequest(ConfigErrorRequest, handleConfigError);
	});

	client.start();

	/* map commands */
	vscode.commands.registerCommand("html-validate.rule-documentation", commands.ruleDocumentation);
}

function handleMissingLibraryError(): void {
	const outputItem: ButtonItem = {
		title: "Go to output",
		id: 1,
	};
	client.warn(
		[
			"",
			"Failed to locate html-validate library.",
			"",
			"html-validate must be installed either in workspace or global. Use one of:",
			"",
			"    nom install --save-dev html-validate",
			"    npm install --global html-validate",
			"",
		].join("\n")
	);
	Window.showErrorMessage(
		`Failed to locate html-validate library, see output for details.`,
		outputItem
	).then((item) => {
		if (item && item.id === outputItem.id) {
			client.outputChannel.show(true);
		}
	});
}

function handleMinVersionError(params: MinVersionErrorParams): void {
	client.warn(
		[
			"Failed to load html-validate library.",
			`Minimum required version is ${params.min} but current version is ${params.cur}`,
			"Please install a later version to continue.",
		].join("\n")
	);
	Window.showErrorMessage(`Failed to load html-validate library, version ${params.min} required.`);
	client.outputChannel.show(true);
}

function handleConfigError(params: ConfigErrorParams): void {
	client.warn(
		[
			"HTML-Validate crashed due to a configuration error.",
			`Validation has been disabled for this document until the issue is resolved.`,
			params.error,
		].join("\n")
	);
	Window.showErrorMessage(
		`HTML-Validate crashed due to a configuration error. Disabled for this document.`
	);
	client.outputChannel.show(true);
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
