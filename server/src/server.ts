/* eslint camelcase: ["error", {"allow": ["__.*__"]}] */
/* eslint-disable sonarjs/no-duplicate-string */

import path from "path";
import {
	createConnection,
	TextDocuments,
	DiagnosticSeverity,
	ProposedFeatures,
	DidChangeConfigurationNotification,
	Files,
	RequestType,
	TextDocumentSyncKind,
} from "vscode-languageserver/node";
import { TextDocument } from "vscode-languageserver-textdocument";
import { URI } from "vscode-uri";
import findUp from "find-up";
import { HtmlValidate, Severity, Message, Config } from "html-validate";

/* semvar lazy-loading interferes with webpack, must force it to bundle it */
import satisfies from "semver/functions/satisfies";
import { ApiContext } from "./api-context";
import { onCodeAction } from "./code-actions";
import { onHover } from "./hover";
import { ExtendedDiagnostic } from "./extended-diagnostic";

const HtmlValidateVersion = "^2.17 || ^3 || ^4 || ^5";

declare const __webpack_require__: typeof require;
declare const __non_webpack_require__: typeof require;

interface HtmlValidateSettings {
	enable: boolean;
	validate: string[];
}

/* eslint-disable-next-line @typescript-eslint/no-empty-interface */
interface MissingLibraryErrorParams {}

interface MinVersionErrorParams {
	min: string;
	cur: string;
}

interface ConfigErrorParams {
	error: string;
}

const MissingLibraryErrorRequest = new RequestType<MissingLibraryErrorParams, void, void>(
	"html-validate/missing-library-error"
);

const MinVersionErrorRequest = new RequestType<MinVersionErrorParams, void, void>(
	"html-validate/min-version-error"
);

const ConfigErrorRequest = new RequestType<ConfigErrorParams, void, void>(
	"html-validate/config-error"
);

const connection = createConnection(ProposedFeatures.all);
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);
const documentEnabled: Map<string, boolean> = new Map();
const documentSettings: Map<string, Thenable<HtmlValidateSettings>> = new Map();
const documentValidator: Map<string, Promise<HtmlValidate | null>> = new Map();
const documentDiagnostics: Map<string, ExtendedDiagnostic[]> = new Map();

const apiContext: ApiContext = {
	getDocumentDiagnostics,
	getDocumentValidator,
	trace,
};

function getDocumentSettings(resource: string): Thenable<HtmlValidateSettings> {
	let result = documentSettings.get(resource);
	if (!result) {
		result = connection.workspace.getConfiguration({
			scopeUri: resource,
			section: "html-validate",
		});
		documentSettings.set(resource, result);
	}

	return result;
}

function getDocumentValidator(resource: string): Promise<HtmlValidate | null> {
	let result = documentValidator.get(resource);
	if (!result) {
		result = getHtmlValidate(resource);
		documentEnabled.set(resource, true);
		documentValidator.set(resource, result);
	}

	return result;
}

async function getDocumentDiagnostics(resource: string): Promise<ExtendedDiagnostic[]> {
	const result = documentDiagnostics.get(resource);
	if (!result) {
		return [];
	}
	return result;
}

function trace(message: string, verbose?: string): void {
	connection.tracer.log(message, verbose);
}

/**
 * Convert from html-validate severity to vscode diagnostic severity
 */
function convertSeverity(severity: Severity): DiagnosticSeverity {
	switch (severity) {
		case 1:
			return DiagnosticSeverity.Warning;
		case 2:
			return DiagnosticSeverity.Error;
		default:
			return DiagnosticSeverity.Error;
	}
}

/**
 * Convert a html-validate error message to vscode diagnostic message.
 */
function convertDiagnostic(
	textDocument: TextDocument,
	config: Config,
	htmlvalidate: HtmlValidate,
	message: Message
): ExtendedDiagnostic {
	const documentation = htmlvalidate.getRuleDocumentation(message.ruleId, config, message.context);
	return {
		severity: convertSeverity(message.severity),
		range: {
			start: textDocument.positionAt(message.offset),
			end: textDocument.positionAt(message.offset + message.size),
		},
		message: message.message,
		code: message.ruleId,
		source: "html-validate",
		documentation,
	};
}

function isValidVersion(version: string | undefined): boolean {
	/* version was exposed in v1.16.0 so older versions will not have this property */
	if (!version) {
		return false;
	}

	/* test against allowed version range */
	return satisfies(version, HtmlValidateVersion);
}

/**
 * Search workspace `html-validate` library.
 */
async function findProjectLibrary(cwd: string): Promise<string | null> {
	connection.tracer.log(`Searching package.json from directory "${cwd}"`);
	let current = cwd;

	while (current !== "/") {
		const pkg = await findUp("package.json", { cwd: current });
		if (!pkg) {
			break;
		}

		const nodePath = path.dirname(pkg);

		try {
			return await Files.resolve("html-validate", nodePath, nodePath, trace);
		} catch (err) {
			/* do nothing */
		}

		current = path.dirname(nodePath);
	}

	return null;
}

/**
 * Search for a global installation of `html-validate` library.
 */
async function findGlobalLibrary(): Promise<string | null> {
	try {
		const globalNpm = Files.resolveGlobalNodePath(trace);
		return await Files.resolve("html-validate", globalNpm, undefined, trace);
	} catch (err) {
		return null;
	}
}

/**
 * Locate `html-validate` library.
 */
async function findLibrary(cwd: string): Promise<string | null> {
	const local = await findProjectLibrary(cwd);
	if (local) {
		return local;
	}

	const global = await findGlobalLibrary();
	if (global) {
		return global;
	}

	return null;
}

/**
 * Find html-validate module from user workspace by traversing the directory
 * structure upwards until a `package.json` file is found.
 */
async function getHtmlValidate(resource: string): Promise<HtmlValidate | null> {
	const dir = path.dirname(resource);
	const modulePath = await findLibrary(dir);

	/* report error if library wasn't found */
	if (!modulePath) {
		connection.tracer.log(`html-validate not found`);
		connection.sendRequest(MissingLibraryErrorRequest, {}).then(undefined, () => {
			/* silently ignore errors */
		});
		return null;
	}

	connection.tracer.log(`html-validate found at "${modulePath}"`);

	const r = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	const module: typeof import("html-validate") = r(modulePath);

	/* validate minimum required version */
	connection.tracer.log(`html-validate is at version "${module.version}"`);
	if (!isValidVersion(module.version)) {
		connection
			.sendRequest(MinVersionErrorRequest, {
				min: HtmlValidateVersion,
				cur: module.version || "unknown",
			})
			.then(undefined, () => {
				/* silently ignore errors */
			});
		return null;
	}

	const { CLI } = module;

	const cli = new CLI();
	return cli.getValidator();
}

async function validateTextDocument(textDocument: TextDocument): Promise<void> {
	const resource = textDocument.uri;
	const settings = await getDocumentSettings(resource);
	const uri = URI.parse(resource);

	documentDiagnostics.set(resource, []);

	/* test if globally disabled for this workspace */
	if (!settings.enable) {
		return;
	}

	/* test if file has been disabled because of internal error */
	const enabled = documentEnabled.get(resource);
	if (enabled === false) {
		connection.tracer.log(
			`Validation disabled for ${textDocument.uri} until configuration issue is resolved.`
		);
		return;
	}

	/* only validate documents with enabled languageId's */
	if (!settings.validate.includes(textDocument.languageId)) {
		connection.tracer.log(
			`skipping html-validate for "${uri.fsPath}" (language: "${textDocument.languageId}")`
		);
		return;
	}

	let htmlvalidate: HtmlValidate;
	try {
		const result = await getDocumentValidator(uri.path);
		if (!result) {
			return;
		}
		htmlvalidate = result;
	} catch (err) {
		connection.tracer.log(`failed to load html-validate for "${uri.fsPath}"`);
		connection.tracer.log(err);
		return;
	}

	/* test if html-validate is configured to validate given filename */
	if (!htmlvalidate.canValidate(uri.fsPath)) {
		connection.tracer.log(
			`skipping html-validate for "${uri.fsPath}" as no transformation has been configured`
		);
		return;
	}

	try {
		const text = textDocument.getText();
		const report = htmlvalidate.validateString(text, uri.fsPath);
		if (report.results.length === 0) {
			connection.sendDiagnostics({ uri: resource, diagnostics: [] });
			return;
		}

		const config = htmlvalidate.getConfigFor(resource);
		const messages = report.results[0].messages;
		const diagnostics = messages.map((entry) =>
			convertDiagnostic(textDocument, config, htmlvalidate, entry)
		);

		documentDiagnostics.set(resource, diagnostics);
		connection.sendDiagnostics({ uri: resource, diagnostics });
	} catch (err) {
		connection.tracer.log(err);
		documentEnabled.set(resource, false);
		connection
			.sendRequest(ConfigErrorRequest, {
				error: err.message,
			})
			.then(undefined, () => {
				/* silently ignore errors */
			});
	}
}

connection.onInitialize(() => {
	return {
		capabilities: {
			textDocumentSync: TextDocumentSyncKind.Full,
			codeActionProvider: true,
			hoverProvider: true,
		},
	};
});

connection.onInitialized(() => {
	connection.client.register(DidChangeConfigurationNotification.type, undefined);
	connection.workspace.onDidChangeWorkspaceFolders(() => {
		documentEnabled.clear();
		documentSettings.clear();
		documentValidator.clear();
		documentDiagnostics.clear();
	});
});

connection.onDidChangeConfiguration(() => {
	documentEnabled.clear();
	documentSettings.clear();
	documentValidator.clear();
	documentDiagnostics.clear();
	documents.all().forEach(validateTextDocument);
});

documents.onDidClose((e) => {
	documentEnabled.delete(e.document.uri);
	documentSettings.delete(e.document.uri);
	documentValidator.delete(e.document.uri);
	documentDiagnostics.delete(e.document.uri);
});

documents.onDidChangeContent((change) => {
	validateTextDocument(change.document);
});

connection.onDidChangeWatchedFiles((_change) => {
	/* remove cached validators to force configuration to be reloaded */
	documentEnabled.clear();
	documentValidator.clear();
	documentDiagnostics.clear();

	/* revalidate after configuration change */
	documents.all().forEach(validateTextDocument);
});

/**
 * Setup listener for code actions.
 */
connection.onCodeAction((params) => onCodeAction.call(apiContext, params));

/**
 * Setup listener for hover popup.
 */
connection.onHover((params) => onHover.call(apiContext, params));

documents.listen(connection);
connection.listen();
