const merge = require("merge-options");

module.exports = function withDefaults(/**@type {import('webpack').Configuration}*/ extConfig) {
	/**@type {import('webpack').Configuration}*/
	const defaultConfig = {
		mode: process.env.NODE_ENV || "development",
		target: "node",

		optimization: {
			minimize: false,
		},

		output: {
			libraryTarget: "commonjs2",
			devtoolModuleFilenameTemplate: "../[resource-path]",
		},
		devtool: "source-map",
		externals: {
			vscode: "commonjs vscode",
		},
		resolve: {
			extensions: [".ts", ".js"],
		},
		node: {
			__dirname: false, // leave the __dirname-behaviour intact
		},

		module: {
			rules: [
				{
					test: /\.ts$/,
					exclude: /node_modules/,
					use: [
						{
							loader: "ts-loader",
							options: {
								compilerOptions: {
									module: "es6",
									moduleResolution: "node",
									sourceMap: true,
								},
							},
						},
					],
				},
			],
		},
	};

	return merge(defaultConfig, extConfig);
};
